create table pessoa (
	codigo int,
	nome varchar(60),
	primary key(codigo)
)

create table compra (
	numero int,
	data date,
	descricao varchar(60),
	codclie int,
	primary key(numero),
	foreign key (codclie) references pessoa(codigo)
)

